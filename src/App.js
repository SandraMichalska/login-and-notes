import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import history from './history';
import * as actions from './store/actions';
import { connect } from 'react-redux';
import { CapiClient } from './api/rekrClient';
import Layout from './containers/Layout/Layout';
import Form from './containers/Form/Form';
import NoteList from './containers/NoteList/NoteList';
import Signout from './containers/Signout/Signout';

class App extends React.Component {
	componentDidMount() {	
		this.props.onAppLoad(new CapiClient("https://rekrutacja.sandbox-m.sowa.pl/api/v1"));
		this.props.onTryAutoSignup(history.push);
	}

	render() {
		let routes = (
			<Switch>
				<Route path="/account" component={Form} />
				<Route path="/signout" component={Signout} />
				<Redirect from='/' to='/account/signin'/>
			</Switch>
		);
		
		if(this.props.isAuthenticated) {
			routes = (
				<Switch>
					<Route path="/notes" exact render={() => <Layout><NoteList/></Layout>} />
					<Route path="/account" component={Form} />
					<Route path="/signout" component={Signout} />
					<Redirect from='/' to='/account/signin'/>
				</Switch>
			);
		}

		return (
			<>
				{routes}
			</>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.signinAndOut.userEmail
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onAppLoad: (client) => dispatch(actions.saveClient(client)),
		onTryAutoSignup: (historyPush) => dispatch(actions.checkState(historyPush)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
