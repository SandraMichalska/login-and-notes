import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';

// komponent przekierowuje na stronę główną po wylogowaniu oraz wysyła akcję, która usuwa dane z sessionStorage
class Signout extends React.Component {
    componentDidMount() {
        this.props.onSignout(this.props.history.push);
    }

    render() {
        return null;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSignout: (historyPush) => dispatch(actions.signout(historyPush))
    }
};

export default connect(null, mapDispatchToProps)(Signout);