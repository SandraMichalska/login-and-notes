import React from 'react';
import { connect } from 'react-redux';

import Navbar from '../../components/Navbar/Navbar';

class Layout extends React.Component {
	render() {
		return (
			<div>
				<Navbar userEmail={this.props.userEmail} />
				{this.props.children}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		userEmail: state.signinAndOut.userEmail
	}
};

export default connect(mapStateToProps)(Layout);