import React from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';
import Note from '../../components/Note/Note';
import './NoteList.scss';

class NoteList extends React.Component {
    state = {
        noteName: "",
        noteText: "",
        isButtonDisabled: true
    }

    componentDidMount() {
        this.loadNotesIfClient();
    }

    componentDidUpdate() {
       this.loadNotesIfClient();
    }
    
    loadNotesIfClient() {
        if(this.props.client) {
            this.props.onLoad(this.props.client);
        }
    }

    handleNoteAddClick = () => {
        this.props.onNoteAddClick(this.props.client, this.state.noteName, this.state.noteText);
    }

    handleNoteNameChange = (e) => {
        const isButtonDisabled = e.target.value === "" || this.state.noteText === "" ? true : false;

        this.setState({ 
            noteName: e.target.value,
            isButtonDisabled
        });
    }

    handleNoteTextChange = (e) => {
        const isButtonDisabled = e.target.value === "" || this.state.noteName === "" ? true : false;

        this.setState({ 
            noteText: e.target.value,
            isButtonDisabled
        });
    }

    render() {
        let noteList;

        // notatki wczytywane z serwera
        if(this.props.noteList) {
            noteList = (
                this.props.noteList.map(note => {
                    const date = note.modified_at.getDate();
                    const month = note.modified_at.getMonth();
                    const year = note.modified_at.getFullYear();

                    const fullDate = `${date}.${month}.${year}`;

                    return <Note
                        key={note.modified_at}
                        name={note.name}
                        text={note.text}
                        modifiedAt={fullDate} />;
                })
            )
        }
        
        return (
            <section className="l-note-list">
                {/* notatka do dodawania nowych notatek */}
                <Note 
                    isButtonDisabled={this.state.isButtonDisabled}
                    onNoteAddClick={this.handleNoteAddClick}
                    onNoteNameChange={this.handleNoteNameChange}
                    onNoteTextChange={this.handleNoteTextChange}
                    cls="l-note--add" />
                {noteList}
            </section>
        );
    }
}

const mapDispatchToProps = dispatch => {
	return {
        onLoad: (client) => dispatch(actions.loadNotes(client)),
        onNoteAddClick: (client, noteName, noteText) => dispatch(actions.addNote(client, noteName, noteText)),
	}
};

const mapStateToProps = state => {
    return {
        client: state.general.client,
        noteList: state.notes.noteList
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteList);
