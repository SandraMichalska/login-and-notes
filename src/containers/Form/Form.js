import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import Signin from '../../components/Signin/Signin';
import Signup from '../../components/Signup/Signup';
import * as actions from '../../store/actions';
import './Form.scss';

class Form extends React.Component {
    refEmailInput = React.createRef(); 
    refPasswordInput = React.createRef(); 
    refPasswordRepeatInput = React.createRef();
    
    state = {
        loginForm: {
            email: {
                elementId: 'email',
                elementType: 'email',
                placeholder: 'Adres e-mail',
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            password: {
                elementId: 'password',
                elementType: 'password',
                placeholder: 'Hasło',
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            },
            passwordRepeat: {
                elementId: 'passwordRepeat',
                elementType: 'password',
                placeholder: 'Powtórz hasło',
                value: '',
                validation: {
                    required: true,
                    minLength: 1,
                    maxLength: 80
                },
                valid: false,
                touched: false
            }
        },
        isFormValid: false
    };


    // sprawdza, czy wpisane hasło zawiera przynajmniej 3 różne znaki
    checkIfAtLeast3DifferentChars(ref) {
        let atLeast3DifferentNums = false;
        let charMap = {};

        for (let char of ref.value) {
            if (charMap.hasOwnProperty(char)) {
                charMap[char]++;
            } else {
                charMap[char] = 1;
            }
        }
        
        if(Object.keys(charMap).length >= 3) {
            atLeast3DifferentNums = true;
        }

        return !ref.validity.patternMismatch && atLeast3DifferentNums;
    }

    validateElement = (inputId, inputData) => {
        let isValid = true;

        if(inputData.validation.required) {
            isValid = inputData.value !== '' && isValid;
        }

        if(inputData.validation.minLength) {
            isValid = inputData.value.length >= inputData.validation.minLength && isValid;
        }

        if(inputData.validation.maxLength) {
            isValid = inputData.value.length <= inputData.validation.maxLength && isValid;
        }
                
        if(inputId === 'email') {
            isValid = !this.refEmailInput.current.validity.patternMismatch && isValid;
        }

        if(inputId === 'password' && this.refPasswordInput.current !== null) {
            isValid = this.checkIfAtLeast3DifferentChars(this.refPasswordInput.current);
        }

        if(inputId === 'passwordRepeat' && this.refPasswordRepeatInput.current !== null) {
            isValid = this.checkIfAtLeast3DifferentChars(this.refPasswordRepeatInput.current) && isValid;
        }

        return isValid;
    }
    
    // metoda waliduje wpisywane wartości pól formularza oraz zapisuje je w stanie
    handleInputChange = (event, inputId, noValidate) => {
        let isFormValid = true;

        const updatedLoginForm = {
            ...this.state.loginForm
        };

        const updatedFormElement = { 
            ...updatedLoginForm[inputId]
        };
        
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.validateElement(inputId, updatedFormElement);

        // sprawdzenie, czy wartości pól z hasłami przy rejestracji są takie same
        if((inputId === "password" || inputId === "passwordRepeat") && !noValidate) {
            let secondInputId;
            
            if(inputId === "password") {
                secondInputId = "passwordRepeat";
            } else {
                secondInputId = "password";
            }

            if(updatedFormElement.value === updatedLoginForm[secondInputId].value) {
                if(updatedFormElement.valid) {
                    updatedLoginForm[secondInputId].valid = true;
                    isFormValid = true;
                } else {
                    isFormValid = false;
                }
            } else {
                updatedFormElement.valid = false;
                updatedLoginForm[secondInputId].valid = false;
                isFormValid = false;
            }
        }

        updatedFormElement.touched = true;
        updatedLoginForm[inputId] = updatedFormElement;

        for(let inputId in updatedLoginForm) {
            // w przypadku logowania nie ma pola z powtórzeniem hasła
            if(inputId === "passwordRepeat" && noValidate) continue;
            isFormValid = updatedLoginForm[inputId].valid && isFormValid;
        }

        this.setState({
            loginForm: updatedLoginForm,
            isFormValid: isFormValid
        });
    }

    handleFormSubmit = (event, propToDispatch) => {
        event.preventDefault();

        propToDispatch(
            this.state.loginForm.email.value, 
            this.state.loginForm.password.value, 
            this.props.client,
            this.props.history.push);
    }

    // metoda resetuje zawartość pól formularza
    handleLinkClick = () => {
        this.props.onLinkClick();

        const state = {...this.state};
        state.isFormValid = false;
        state.loginForm.email.value = "";
        state.loginForm.password.value = "";
        state.loginForm.passwordRepeat.value = "";
        state.loginForm.email.valid = false;
        state.loginForm.password.valid = false;
        state.loginForm.passwordRepeat.valid = false;
        state.loginForm.email.touched = false;
        state.loginForm.password.touched = false;
        state.loginForm.passwordRepeat.touched = false;
    
        this.setState({
            loginForm: state.loginForm,
            isFormValid: state.isFormValid
        });
    }

    prepareAccountRoutes = () => {
        const accountRoutes = [];

        const accountRoutesData = [
            {
                accountPath: 'signin',
                component: <Signin
                    loginFormData={this.state.loginForm}
                    refEmailInput={this.refEmailInput}
                    errorSignin={this.props.errorSignin}
                    isButtonDisabled={!this.state.isFormValid}
                    onSubmit={this.handleFormSubmit}
                    onInputChange={this.handleInputChange}
                    onLinkClick={this.handleLinkClick}
                    propToDispatch={this.props.onSignin} />
            },
            {
                accountPath: 'signup',
                component: <Signup
                    loginFormData={this.state.loginForm} 
                    refEmailInput={this.refEmailInput} 
                    refPasswordInput={this.refPasswordInput} 
                    refPasswordRepeatInput={this.refPasswordRepeatInput} 
                    errorSignup={this.props.errorSignup}
                    isButtonDisabled={!this.state.isFormValid}
                    onSubmit={this.handleFormSubmit}
                    onInputChange={this.handleInputChange}
                    onLinkClick={this.handleLinkClick}
                    propToDispatch={this.props.onSignup} />
            }
        ];

        accountRoutesData.map(data => {
            return accountRoutes.push(
               <Route
                    path={`${this.props.match.path}/${data.accountPath}`}
                    render={(props) => data.component}
                    key={data.accountPath} />
            );
        });

        return accountRoutes.map(route => route);
    }

    componentDidMount = () => {
        this.props.history.push('/account/signin');
    }
      
    render() {
        return (
            <div>
                {this.prepareAccountRoutes()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        client: state.general.client,
        errorSignin: state.signinAndOut.error,
        errorSignup: state.signup.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSignin: (email, password, client, historyPush) => dispatch(actions.signin(email, password, client, historyPush)),
        onSignup: (email, password, client, historyPush) => dispatch(actions.signup(email, password, client, historyPush)),
        onLinkClick: () => dispatch(actions.clearErrors())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);