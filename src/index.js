import React from 'react';
import ReactDOM from 'react-dom';
import history from './history';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import App from './App';
import generalReducer from './store/reducers/general';
import signupReducer from './store/reducers/signup';
import signinAndOutReducer from './store/reducers/signinAndOut';
import notes from './store/reducers/notes';
import { watchLogin } from './store/sagas';
import './index.scss';

const rootReducer = combineReducers({
    general: generalReducer,
    signup: signupReducer,
    signinAndOut: signinAndOutReducer,
    notes: notes
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(watchLogin);

const app = (
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
