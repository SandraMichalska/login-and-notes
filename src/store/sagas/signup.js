import { put } from "redux-saga/effects";

import * as actions from "../actions";
import { UserSignup } from "../../api/rekrClient";

/*
    rejestracja użytkownika - wysyłanie żądania do serwera, zapisanie danych w sessionStorage, 
    przekierowanie do widoku notatek oraz obsługa błędów
*/
export function* signupSaga(action) {
    yield put(actions.signupStart());

    const signupData = {
        email: action.email,
        password: action.password
    };

    const response = yield action.client
        .executeSingle(new UserSignup(signupData.email, signupData.password));

    if (response.status === 200) {
        console.log(`id użytkownika = ${response.data.user_id}`);
        yield sessionStorage.setItem('userEmail', signupData.email);
        yield put(actions.signupSuccess());

        // automatyczne logowanie po rejestracji
        yield put(actions.signin(signupData.email, signupData.password, action.client, action.historyPush));

        yield action.historyPush('/notes');
    } else {
        console.log(`błąd: ${response.status} ${response.message}`);
        yield put(actions.signupFailure(`${response.message}`)); 
    }
}