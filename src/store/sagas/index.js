import { takeEvery } from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';
import { signupSaga } from './signup';
import { signinSaga, signoutSaga } from './signinAndOut';
import { clearErrorsSaga, checkTimeoutSaga, checkStateSaga } from './general';
import { loadNotesSaga, addNoteSaga } from './notes';

export function* watchLogin() {
    yield takeEvery(actionTypes.SIGNIN_SAGA, signinSaga);
    yield takeEvery(actionTypes.SIGNUP_SAGA, signupSaga);
    yield takeEvery(actionTypes.SIGNOUT_SAGA, signoutSaga);
    yield takeEvery(actionTypes.CHECK_TIMEOUT_SAGA, checkTimeoutSaga);
    yield takeEvery(actionTypes.CHECK_STATE_SAGA, checkStateSaga);
    yield takeEvery(actionTypes.CLEAR_ERRORS_SAGA, clearErrorsSaga);
    yield takeEvery(actionTypes.LOAD_NOTES_SAGA, loadNotesSaga);
    yield takeEvery(actionTypes.ADD_NOTE_SAGA, addNoteSaga);
}