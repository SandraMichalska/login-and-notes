
import { put } from "redux-saga/effects";

import * as actions from "../actions";
import { SessionCreate } from "../../api/rekrClient";

/*
    logowanie użytkownika - wysyłanie żądania do serwera, zapisanie danych w sessionStorage i stanie, 
    przekierowanie do widoku notatek oraz obsługa błędów
*/
export function* signinSaga(action) {
    yield put(actions.signinStart());
    
    const signinData = {
        email: action.email,
        password: action.password
    };

    const response = yield action.client
        .withLogin(signinData.email, signinData.password)
        .executeSingle(new SessionCreate());

    if (response.status === 200) {
        console.log(`id sesji = ${response.data.session_id}`);
        console.log(`klucz sesji = ${response.data.session_key}`);
        console.log(`wygasa = ${response.data.expires_at}`);
        yield sessionStorage.setItem('userEmail', signinData.email);
        yield sessionStorage.setItem('sessionId', response.data.session_id);
        yield sessionStorage.setItem('sessionKey', response.data.session_key);
        yield sessionStorage.setItem('expirationTime', response.data.expires_at);
        yield put(actions.signinSuccess(signinData.email, response.data.expires_at));
        yield put(actions.checkTimeout(response.data.expires_at, action.historyPush));
        yield action.historyPush('/notes');
    }
    else {
        console.log(`błąd: ${response.status} ${response.message}`);
        yield put(actions.signinFailure(`${response.message}`)); 
    }
}

/*
    wylogowywanie użytkownika - usunięcie danych z sessionStorage i stanu
*/
export function* signoutSaga(action) {
    yield put(actions.signoutSuccess());

    yield sessionStorage.removeItem('sessionId');
    yield sessionStorage.removeItem('sessionKey');
    yield sessionStorage.removeItem('expirationTime');
    yield sessionStorage.removeItem('userEmail');

    yield action.historyPush('/');
}