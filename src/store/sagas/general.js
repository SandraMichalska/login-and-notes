
import { put } from "redux-saga/effects";
import * as actions from "../actions";

export function* clearErrorsSaga(action) {
    yield put(actions.clearSigninErrors());
    yield put(actions.clearSignupErrors());
}

export function* checkTimeoutSaga(action) {
    yield setTimeout(() => {
        action.historyPush('/signout');
    }, new Date(action.expirationTime) - new Date());
}

export function* checkStateSaga(action) {
    const userEmail = sessionStorage.getItem('userEmail');
    const expirationTime = new Date(sessionStorage.getItem('expirationTime'));

    if(userEmail) {
        if(expirationTime < new Date()) {
            action.historyPush('/signout');
        } else {
            setTimeout(() => {
                action.historyPush('/signout');
            }, expirationTime);
        }
    }
}