import { put } from "redux-saga/effects";

import { NoteList, NoteTake } from "../../api/rekrClient";
import * as actions from "../actions";

export function* loadNotesSaga(action) {
    const response = yield action.client
        .withSession(sessionStorage.getItem('sessionId'), sessionStorage.getItem('sessionKey'))
        .executeSingle(new NoteList());

    if (response.status === 200) {
        const results = response.data.results;

        // for (let i = 0; i < results.length; i++) {
            // const result = results[i];
            // console.log(`notatka "${result.name}":`);
            // console.log(result.text);
            // console.log(`(napisana: ${result.modified_at})`);
        // }
        yield put(actions.loadNotesSuccess(results));
    } else {
        console.log(`błąd: ${response.status} ${response.message}`);
    }
}

export function* addNoteSaga(action) {
    const response = yield action.client
        .withSession(sessionStorage.getItem('sessionId'), sessionStorage.getItem('sessionKey'))
        .executeSingle(new NoteTake(action.noteName, action.noteText));

    if(response.status !== 204) {
        console.log(`błąd: ${response.status} ${response.message}`);
    };
}