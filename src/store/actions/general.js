import * as actionTypes from './actionTypes';

export const saveClient = (client) => {
    return {
        type: actionTypes.SAVE_CLIENT,
        client
    }
};

export const clearErrors = () => {
    return {
        type: actionTypes.CLEAR_ERRORS_SAGA
    }
};

export const checkTimeout = (expirationTime, historyPush) => {
    return {
        type: actionTypes.CHECK_TIMEOUT_SAGA,
        expirationTime,
        historyPush
    }
};

export const checkState = (historyPush) => {
    return {
        type: actionTypes.CHECK_STATE_SAGA,
        historyPush
    }
};