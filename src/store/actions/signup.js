import * as actionTypes from './actionTypes';

export const signup = (email, password, client, historyPush) => {
    return {
        type: actionTypes.SIGNUP_SAGA,
        email,
        password,
        client,
        historyPush
    }
};

export const signupStart = () => {
    return {
        type: actionTypes.SIGNUP_START
    }
};

export const signupSuccess = () => {
    return {
        type: actionTypes.SIGNUP_SUCCESS
    }
};

export const signupFailure = (error) => {
    return {
        type: actionTypes.SIGNUP_FAILURE,
        error
    }
};

export const clearSignupErrors = () => {
    return {
        type: actionTypes.CLEAR_SIGNUP_ERRORS
    }
};