import * as actionTypes from './actionTypes';

export const loadNotes = (client) => {
    return {
        type: actionTypes.LOAD_NOTES_SAGA,
        client
    }
};

export const loadNotesSuccess = (noteList) => {
    return {
        type: actionTypes.LOAD_NOTES_SUCCESS,
        noteList
    }
};

export const addNote = (client, noteName, noteText) => {
    return {
        type: actionTypes.ADD_NOTE_SAGA,
        client,
        noteName,
        noteText
    }
};