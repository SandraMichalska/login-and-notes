import * as actionTypes from './actionTypes';

export const signin = (email, password, client, historyPush) => {
    return {
        type: actionTypes.SIGNIN_SAGA,
        email,
        password,
        client,
        historyPush
    }
};

export const signinStart = () => {
    return {
        type: actionTypes.SIGNIN_START
    }
};

export const signinSuccess = (userEmail, expirationTime) => {
    return {
        type: actionTypes.SIGNIN_SUCCESS,
        expirationTime,
        userEmail
    }
};

export const signinFailure = (error) => {
    return {
        type: actionTypes.SIGNIN_FAILURE,
        error
    }
};

export const signout = (historyPush) => {
    return {
        type: actionTypes.SIGNOUT_SAGA,
        historyPush
    }
};

export const signoutSuccess = (error) => {
    return {
        type: actionTypes.SIGNOUT_SUCCESS,
        userEmail: null
    }
};

export const clearSigninErrors = () => {
    return {
        type: actionTypes.CLEAR_SIGNIN_ERRORS
    }
};