import * as actionTypes from '../actions/actionTypes';

const initialState = {
    noteList: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.LOAD_NOTES_SUCCESS:
            return {
                ...state,
                noteList: action.noteList
            };
        default:
            return state;
    }
};

export default reducer;