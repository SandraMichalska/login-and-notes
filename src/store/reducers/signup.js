import * as actionTypes from '../actions/actionTypes';

const initialState = {
    error: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SIGNUP_START:
            return {
                ...state,
                error: null
            };
        case actionTypes.SIGNUP_SUCCESS:
            return {
                ...state,
                error: null
            };
        case actionTypes.SIGNUP_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case actionTypes.CLEAR_SIGNUP_ERRORS:
            return {
                ...state,
                error: null
            };
        default:
            return state;
    }
};

export default reducer;