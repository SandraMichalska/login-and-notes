import * as actionTypes from '../actions/actionTypes';

const initialState = {
    userEmail: sessionStorage.getItem('userEmail'),
    expirationTime: sessionStorage.getItem('expirationTime'),
    error: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SIGNIN_START:
            return {
                ...state,
                error: null
            };
        case actionTypes.SIGNIN_SUCCESS:
            return {
                ...state,
                userEmail: action.userEmail,
                expirationTime: action.expirationTime,
                error: null
            };
        case actionTypes.SIGNIN_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case actionTypes.SIGNOUT_SUCCESS:
            return {
                ...state,
                userEmail: null
            };
        case actionTypes.CLEAR_SIGNIN_ERRORS:
            return {
                ...state,
                error: null
            };
        default:
            return state;
    }
};

export default reducer;