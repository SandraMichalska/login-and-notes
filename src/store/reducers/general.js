import * as actionTypes from '../actions/actionTypes';

const initialState = {
    client: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SAVE_CLIENT:
            return {
                ...state,
                client: action.client
            };
        default:
            return state;
    }
};

export default reducer;