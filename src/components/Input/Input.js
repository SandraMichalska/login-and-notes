import React from 'react';
import './Input.scss';

const Input = props => {
    let inputElement;
    let invalidCls = '';
    let pattern;
    let ref;

    /*
        Mail musi składać się z ciągu znaków, znaku @, kolejnego ciągu znaków, kropki oraz przynajmniej 2 liter,
        np. znaki@znaki.domena
    */
    const patternEmail = "[a-z0-9._%+-]+@[a-z0-9-]+\\.[a-z]{2,}$";

    /*
        Hasło musi mieć co najmniej 7 znaków (w tym przynajmniej 3 różne) oraz zawierać:
        - przynajmniej 1 wielką literę,
        - przynajmniej 1 cyfrę lub
        - przynajmniej 1 symbol (#?!@$%^&*-)
    */
    const patternPassword = "^((?=.*?[A-Z])|(?=.*?[0-9])|(?=.*?[#?!@$%^&*-]))(?=.*?[a-z]).{7,}$";
        
    if(props.invalid && props.touched) {
        invalidCls = "l-input__elem--invalid";
    }
    
    if(props.elementId === "email") {
        ref = props.refEmailInput;
        pattern = patternEmail;
    } else if(props.elementId === "password" && props.validate) {
        ref = props.refPasswordInput;
        pattern = patternPassword;
    } else if(props.elementId === "passwordRepeat" && props.validate) {
        ref = props.refPasswordRepeatInput;
        pattern = patternPassword;
    }

    inputElement = <input
        className={`l-input__elem ${invalidCls}`} 
        type={props.elementType}
        placeholder={props.placeholder}
        value={props.value}
        ref={ref}
        pattern={pattern}
        onChange={props.changed}/>;

    return (
        <div className="l-input">
            {inputElement}
        </div>
    );
};
 
export default Input;