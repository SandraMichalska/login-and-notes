import React from 'react';

import Button from '../Button/Button';
import './Note.scss';

const Note = (props) => {
    let noteType = (
        <div 
            className={`l-note ${props.cls}`}>
            <textarea className="l-note__textarea"
                value={props.name} 
                placeholder="Wpisz tytuł" 
                rows="1"
                onChange={(e) => props.onNoteNameChange(e)} />
            <textarea className="l-note__textarea"
                value={props.name} 
                placeholder="Wpisz notatkę" 
                rows="8"
                onChange={(e) => props.onNoteTextChange(e)} />
            <Button 
                className="l-note__add-txt"
                isButtonDisabled={props.isButtonDisabled}
                cls={props.isButtonDisabled ? "l-button__add-note l-button__add-note--disabled" : "l-button__add-note"}
                clicked={props.onNoteAddClick}>Dodaj notatkę</Button>
        </div>
    );

    if(props.name) {
        noteType = (
            <div 
                className="l-note">
                <p className="l-note__txt l-note__txt--name">{props.name}</p>
                <p className="l-note__txt">{props.text}</p>
                <p className="l-note__txt l-note__txt--last-modified">Ostatnio edytowana: {props.modifiedAt}</p>
            </div>
        );
    }

    return noteType;
};

export default Note;