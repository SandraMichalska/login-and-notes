import React from 'react';

import NavItems from './NavItems/NavItems';
import './Navbar.scss';

const Navbar = (props) => (
	<section className="l-nav">
		<nav className="l-nav__inner">
			<NavItems userEmail={props.userEmail}/>
		</nav>
	</section>
);

export default Navbar;
