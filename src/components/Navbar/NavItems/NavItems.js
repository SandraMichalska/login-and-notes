
import React from 'react';

import NavItem from '../NavItem/NavItem';

const NavItems = (props) => (
    <ul className="l-nav__list">
        <NavItem link="">{props.userEmail}</NavItem>
        <NavItem link="/signout">Wyloguj</NavItem>
    </ul>
);

export default NavItems;