import React from 'react';
import { NavLink } from 'react-router-dom';

const NavItem = (props) => (
    <li className="l-nav__list-item">
        <NavLink className="l-nav__link" to={props.link}>
            {props.children}
        </NavLink>
    </li>
);

export default NavItem;