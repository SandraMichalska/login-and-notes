import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';

const Signup = props => {
    const formElements = [];
    
    for(let key in props.loginFormData) {
        formElements.push({
            config: props.loginFormData[key]
        });
    }

    const form = formElements.map(formElement => (
        <Input
            key={formElement.config.elementId}
            elementId={formElement.config.elementId}
            elementType={formElement.config.elementType}
            placeholder={formElement.config.placeholder}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            touched={formElement.config.touched}
            refEmailInput={
                formElement.config.elementId === "email"
                    ? props.refEmailInput
                    : undefined
            }
            refPasswordInput={
                formElement.config.elementId === "password"
                    ? props.refPasswordInput
                    : undefined
            }
            refPasswordRepeatInput={
                formElement.config.elementId === "passwordRepeat"
                    ? props.refPasswordRepeatInput
                    : undefined
            }
            validate={true}
            changed={(event) => props.onInputChange(event, formElement.config.elementId, false)}/>
    ));

    let errorMsgElement = (             
        <p className="l-form__info">
            Wpisane hasła powinny być takie same, mieć co najmniej 7 znaków (w tym co najmniej 3 różne) oraz spełniać przynajmniej jeden z poniższych warunków:<br/>
            - przynajmniej 1 wielka litera<br/>
            - przynajmniej 1 cyfra<br/>
            - przynajmniej 1 symbol (#?!@$%^&*-)
        </p>
    );
    
    if(props.errorSignup !== null) {
        errorMsgElement = <p className="l-form__error-msg">{props.errorSignup}</p>;
    }

    return (
        <section className="l-form">
            <form 
                className="l-form__form"
                onSubmit={(event) => props.onSubmit(event, props.propToDispatch)}>
                <h1 className="l-form__heading">Rejestracja</h1>
                {errorMsgElement}
                {form}
                <Button 
                    isButtonDisabled={props.isButtonDisabled}
                    cls={props.isButtonDisabled ? "l-button--disabled" : ""}>Zarejestruj się</Button>
                <Link 
                    className="l-form__link"
                    to="/account/signin"
                    onClick={props.onLinkClick}>Zaloguj się</Link>
            </form>
        </section>
    );
};

export default Signup;