import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';

const Signin = props => {
    const formElements = [];

    for(let key in props.loginFormData) {
        formElements.push({
            config: props.loginFormData[key]
        });
    }

    const form = formElements.map(formElement => {
        // w przypadku logowania nie ma trzeciego pola (powtórzenie hasła)
        if(formElement.config.elementId === "passwordRepeat") return null;

        return (
            <Input
                key={formElement.config.elementId}
                elementId={formElement.config.elementId}
                elementType={formElement.config.elementType}
                placeholder={formElement.config.placeholder}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                touched={formElement.config.touched}
                refEmailInput={
                    formElement.config.elementId === "email"
                        ? props.refEmailInput
                        : undefined
                }
                validate={
                    formElement.config.elementId === "password"
                        ? false
                        : true
                }
                changed={(event) => props.onInputChange(event, formElement.config.elementId, true)}/>
        )
    });

    return (
        <section className="l-form">
            <form 
                className="l-form__form" 
                onSubmit={(event) => props.onSubmit(event, props.propToDispatch)}>
                <h1 className="l-form__heading">Logowanie</h1>
                <p className="l-form__error-msg">{props.errorSignin}</p>
                {form}
                <Button
                    isButtonDisabled={props.isButtonDisabled} 
                    cls={props.isButtonDisabled ? "l-button--disabled" : ""}>Zaloguj się</Button>
                <Link 
                    className="l-form__link" 
                    to="/account/signup"
                    onClick={props.onLinkClick}>Zarejestruj się</Link>
            </form>
        </section>
    );
};

export default Signin;