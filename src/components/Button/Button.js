import React from 'react';

import './Button.scss';

const Button = (props) => (
  <>
    <button
        onClick={props.clicked}
        className={`l-button ${props.cls}`}
        disabled={props.isButtonDisabled}>
        {props.children}
    </button>
  </>
);

export default Button;
